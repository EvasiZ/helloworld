import * as React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilter} from '@fortawesome/free-solid-svg-icons';
import SearchText from './SearchText'

library.add(faFilter)

interface IHeaderState{
    collapsed: boolean;
    searchText: string;
}

class Header extends React.Component<{}, IHeaderState> {
    constructor (props: {}) {
        super(props);
        this.state = {collapsed: true, searchText: ""}
        //change state
    }

    public onFilterClick = () => {
        this.setState({collapsed: !!!this.state.collapsed})
    }

    public onSearchTextChange = (event:React.ChangeEvent<HTMLInputElement>) => {
        this.setState({searchText: event.target.value})
    }
    public render() {
        return (<div className="header">
                    <p>Hola Mundo: {this.state.searchText}</p>
                    <a className="btn btn-success" onClick={this.onFilterClick}>
                        <FontAwesomeIcon icon="filter" />
                    </a>
                    {!!!this.state.collapsed ? 
                        <div className="filterForm">
                                <SearchText onSearchTextChange={this.onSearchTextChange}/>
                        </div>
                    : null}
                </div>);
    }
}

export default Header;